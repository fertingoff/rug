class Task < ActiveRecord::Base
	validates_presence_of :name 
	belongs_to :project



	default_scope  {order(created_at: :desc)} 

end
