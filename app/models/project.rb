class Project < ActiveRecord::Base
	has_many :tasks
    include ActiveModel::Validations

	validates_presence_of :name
end
