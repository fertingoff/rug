class ProjectsController < ApplicationController

	before_action :common_individual_variables, only: [:show, :destroy, :update, :edit]
	before_action :authenticate_user!

	def index
		@projects_all = Project.all 
	end	


	def common_individual_variables
		@project = Project.find(params[:id])
	end

	def new
		@project = Project.new
	end 	

	def create
	    @project = Project.new(project_params)
	    @project.save
	    redirect_to root_path
	end

	def edit
	end


	def update
		@project = Project.find(params[:id])
		  if @project.update(project_params)
		    redirect_to root_path
		  else
		    render 'edit'
		  end
		end

	def show
	end

	def destroy
		@project.destroy 
		redirect_to root_path 
	end

  private

  def project_params
    params.require(:project).permit(:name, :id)
  end
end
