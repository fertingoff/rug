class TasksController < ApplicationController

	protect_from_forgery except: :create	

	before_action :common_multiply_variables, only: [:index, :show, :destroy]
	before_action :common_individual_variables, only: [:show, :destroy, :update, :edit]
	before_action :authenticate_user!

	def common_multiply_variables
		@tasks_all = Task.all.order(created_at: :asc)
		@projects_all = Project.all.order(created_at: :asc) 
	end

	def common_individual_variables
		@task = Task.find(params[:id])
	end


	def index
		# binding.pry
		respond_to do |format|
			format.html { @tasks = "Task.all" }
			format.json { render json: "I am a response" }
		end
	end

	def edit
		# redirect_to @task
	end

	# binding.pry
	def create
		# binding.pry
		new_task = Task.create({name: params["name"], due_date: params["due_date"], project_id: params["project_id"]})
		respond_to do |format|
			format.json { render json: new_task}
		end
	end

	def update
	 
	  if @task.update(task_params)
	    redirect_to root_path
	  else
	    render 'edit'
	  end
	end

	def show
	end

	

	def destroy
		@task.destroy 

		redirect_to root_path 
	end

	def create_project
		new_project = Project.create({name: params["name"]})
		respond_to do |format|
			format.json { render json: new_project}
		end
end

	private

	def task_params
		params.require(:task).permit(:name, :due_date, :completed)
	end	



end
