class RenameColumnTasksTaskTextName < ActiveRecord::Migration
  def change
  	rename_column :tasks, :task_text, :name 
  end
end
