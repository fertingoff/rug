#encoding: utf-8
Task.delete_all
Project.delete_all

Project.create({name: "Complete the task for Ruby Garage", id: 1})
Project.create({name: "For Home", id: 2})

statuses = %w[hold active potponed completed]
second_project_tasks = ["Repair an obsolete cassete-player", "Blow a room", "Call gammer", "Buy a decoction", "Add js everywere", "Hinschreiben HTML und CSS", "Examine this digital manuscript very attentively ", "Open this sketch in your favorite app"]
8.times do Task.create({
	name: second_project_tasks.shift,
	due_date: Time.now + rand(7).day,
	status: statuses.sample,
	project_id: second_project_tasks.size < 4 ? 1 : 2
	})	
end
